



//Q1. Create 2 files simultaneously (without chaining).
//Wait for 2 seconds and starts deleting them one after another. (in order)

const fs = require("fs");
const path = require("path");
const { promisify } = require("util");

const promiseReadFile = promisify(fs.readFile);
const promiseWriteFile = promisify(fs.writeFile);
const promiseDeleteFile = promisify(fs.unlink);


async function problem1(files) {
    let a = await Promise.all(files.map(file => promiseWriteFile(file, `my ${file}`)))
    console.log("files created")
    setTimeout(() => {
        console.log("files deleteing started")
        promiseDeleteFile(path.join(__dirname, './file1'), (err) => {
            if (err) {
                console.log("err")
            }
            else {

                promiseDeleteFile(path.join(__dirname, './file2'), (err) => {
                    if (err) {
                        console.log(err)
                    }
                    else {
                        console.log("files deleted")
                    }
                })
            }
        })

    }, 2000);

}
let files = ['file1', 'file2']
problem1(files)


/*Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file
Delete the original file 
Using promise chaining
*/

function problem2() {

    promiseReadFile('lipsum.txt')
        .then((data) => {
            console.log("lipsum File reading complete");
            return promiseWriteFile(path.join(__dirname, './newfile.txt'), data)
        })
        .then(() => {
            console.log("data is copied to new file");
            return promiseDeleteFile(path.join(__dirname, './lipsum.txt'));
        })
        .then(() => {
            console.log('original file deleted')
        }).catch((err) => {
            console.error(err)
        });
}

problem2()



